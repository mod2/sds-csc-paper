\contentsline {section}{\numberline {0.1}Lessons in Mapping}{1}{section.0.1}
\contentsline {chapter}{Bibliography}{3}{chapter*.2}
\contentsline {chapter}{Appendices}{5}{section*.3}
\contentsline {chapter}{\numberline {A}Computer Code}{6}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}\textsc {modflow}{}-2005 to \textsc {ParFlow}{} Crosswalk}{6}{section.1.A.1}
\contentsline {section}{\numberline {A.2}{TF}-{IDF} and Cosine\nobreakspace {}Similarity Module}{7}{section.1.A.2}
\contentsline {chapter}{\numberline {B}Licenses}{11}{Appendix.1.B}
\contentsline {section}{\numberline {B.1}{M}odified\nobreakspace {}{BSD} License}{11}{section.1.B.1}
\contentsline {section}{\numberline {B.2}{C}reative\nobreakspace {}{C}ommons {A}ttribution 4.0 {I}nternational {P}ublic\nobreakspace {}{L}icense}{11}{section.1.B.2}
