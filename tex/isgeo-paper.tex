\documentclass[12pt,letterpaper,twoside]{report}


	\usepackage[numbers]{natbib}
	\usepackage[linktoc=all,backref]{hyperref}
	\usepackage[nottoc]{tocbibind}
	\usepackage[toc,page]{appendix}
	\usepackage{sectsty}
	\usepackage[raggedright]{titlesec}
	\usepackage[font=small,labelfont=bf,justification=centering]{caption}
	\usepackage{listings}
	\usepackage{color}
	\usepackage{fullpage}
	\usepackage{multicol}

	\setlength{\columnseprule}{1pt}
	\setlength{\columnsep}{13pt}

	\newcommand*{\MODFLOW}{\textsc{modflow}}
	\newcommand*{\ParFlow}{\textsc{ParFlow}}

	\DeclareCaptionFont{white}{ \color{white} }
	\DeclareCaptionFormat{listing}{
		\colorbox[cmyk]{ 0.43,0.35,0.35,0.01 }{
			\parbox{\textwidth}{\hspace{15pt}#1#2#3}
		}
	}
	\captionsetup[lstlisting]{												%
		format=listing,														%
		labelfont=white,													%
		textfont=white,														%
		singlelinecheck=false,												%
		margin=0pt,															%
		font={bf,footnotesize},												%
	}

	\lstset{																%
		backgroundcolor=\color{white},										%
		breakatwhitespace=false,											%
		breaklines=true,													%
		postbreak=\mbox{\textcolor{red}{\space\space\space\space$\hookrightarrow$}\space},%
		captionpos=t,														%
		frame=L,															%
		keepspaces=true,													%
		rulecolor=\color{black},											%
		showspaces=false,													%
		showstringspaces=false,												%
		showtabs=false,														%
		tabsize=4,															%
		title=\lstname,														%
	}

	\lstdefinestyle{code}{													%
		basicstyle=\footnotesize\ttfamily,									%
		commentstyle=\color{blue},											%
		keywordstyle=\color{green},											%
		numbers=left,														%
		numbersep=10pt,														%
		numberstyle=\tiny\color{red},										%
		stepnumber=5,														%
		stringstyle=\color{magenta},										%
	}

	\lstdefinestyle{license}{												%
		basicstyle=\scriptsize\ttfamily,									%
		escapeinside={\%*}{*)},												%
		firstline=4,														%
	}


\begin{document}


	\pagenumbering{roman}


	{
		\clearpage\null\vfill
		\pagestyle{empty}
		\begin{minipage}[b]{0.9\textwidth}
			\footnotesize\centering
			\setlength{\parskip}{0.5\baselineskip}
			Copyright \copyright 2017, Daniel Hardesty~Lewis\par
			All rights reserved.\par
			Redistribution and use of this computer code in source and binary
				forms, with or without modification, are permitted provided
				that the conditions of the {M}odified~{BSD}~{L}icense are
				met.\par
			Permission is granted to copy, distribute, and\slash or modify this
				document under the terms of the {C}reative~{C}ommons
				{A}ttribution 4.0 {I}nternational {P}ublic~{L}icense.
		\end{minipage}
		\vspace*{2\baselineskip}
	}


	\tableofcontents
	\newpage


	\begin{multicols}{2}
	\raggedright

	\section{Lessons in Mapping}
	\label{sec:lessons}
	\pagenumbering{arabic}

	\paragraph*{}
	Difficulty surrounds the manual mapping of the variables of \MODFLOW{}
		({MF}) or \ParFlow{} ({PF}) to the ontology, {G}eoscience
		{S}tandard~{N}ames ({GSN}).
	This entails the expert eye of a geoscientist combing by hand through the
		information available upon each potential variable present in the {GSN}
		to distinguish whether it indeed matches the definition of one from the
		groundwater modelling software in question.
	Often, it is the case that the terms employed to describe a certain
		phenomenon differ in a fundamental regard from geoscientist to
		geoscientist.
	Commonly found in the {GSN} are seemingly similar variables, which match in
		all but a few essential aspects.
	Such nuances preclude from even the attempt the researcher who may not be
		so well versed in the geosciences.\footnote{The work contained herein,
		inclusive of this written section, continues to be documented under the
		following group of Git repositories:\par
		\url{https://gitlab.com/mod2}}

	\paragraph*{}
	In each variable that it is possible, the adherance to the spoke-hub
		architecture requisited by the utilisation of an ontology resolves to a
		much simpler computational complexity of $O(n)$ the problem of mapping
		between models.
	The variables of the {GSN} rarely find themselves outside of the domain;
		most of them relate natural quantities which describe the physics of
		the geological system at hand.
	One notable exception lies in those variables tied to concepts which the
		vast majority of modelling programs are expected to invoke, such as
		that of a computational grid.
	Variables in a groundwater model which adhere to neither of these
		categories are unlikely to be represented in this ontology.
	As such, there exist many in \MODFLOW{} which require a direct mapping to
		\ParFlow{} -- or any other software, for that matter.\footnote{In each
		of these variables, the complexity of the mapping remains at $O(n^2)$.}

	\paragraph*{}
	In fact, {MF} is rife with variables which assert a great importance in its
		function but stand outside of these categories of the geophysical or of
		the common model.
	One example of such an idiosyncracy is {MF}'s concept of a stress period.
	Designed to simplify the treatment of phenomenoma over time, this variable
		describes the periodicity against which the elements of a model are
		assumed to be constructed.
	Other models, including {PF}, do not uphold as central or essential such a
		repetitive sense of time.
	Further detail regarding this concept lies in the section upon procedures
		within the latest manual of \MODFLOW{} \cite{harbaugh05}.
	As another example, {MF} maintains a reputation for divising differing
		scenarios by way of a flag.
	It strays from the norm in the sheer number of non-binary flags upon which
		it depends.
	Other groundwater simulators tend towards separating such flags into binary
		ones or invoking another variable whose appearance implies the desired
		context.
	Instances find themselves throughout the chapter upon input instructions
		within the manual.

	\paragraph*{}
	Despite how lengthily the {G}eoscience {S}tandard~{N}ames extends their
		predecessor, they nowhere near completeness \cite{peckham14}.
	The well-used package in {MF} (or section in {PF}) which pertains to wells
		stands as one of many known sets of variables which are non-existent in
		the {GSN}.
	Other man-made phenomena likely face a similar fate, for the moment.
	For reference, a section upon the Well Package in \MODFLOW{}'s
		input~instructions details some of the missing variables; the
		corresponding section within \ParFlow{}'s manual is positioned within
		the description of the main input~file \cite{maxwell16}.
	Unlike more traditional ontologies, the rules which found the {GSN} are
		transparent and concise.
	In principle, any competent party could construct a new standard~name.
	This procurement system only falls short in terms of efficiency in that it
		depends upon the central authority of its current maintainer for the
		ultimate approval.

	\paragraph*{}
	The semi-automatic analysis, term-frequency/inverse-document-frequency
		({TF}-{IDF}), combats the drudgery of the manual approach
		\cite{luhn57} \cite{jones72}.
	In our case, each manual of {MF} and {PF} suits a corpus, in which the
		documents are designated as the definitions of each variable.
	A document may then be represented as a vector of {TF}-{IDF} values
		for all of the words of both corpora.
	Each variable of a modeller matches one of the other by exhibiting the
		greatest cosine similarity with it.
	With the returned pairs, a geoscientist is left simply to verify the
		results.
	Unfortunately, this method currently demands human intervention in
		pre-processing to render fit for use the manuals.
	An implementation of the aforementioned in the Perl programming~language
		is included in the appendix, at \ref{ch:code},~\nameref{ch:code}
		\cite{perl17}.

	\paragraph*{}
	Further automation of the mapping process remains as work to be conducted.
	The application of natural~language processing tools towards the specific
		purpose of discerning variables and their definitions from the
		documentation of groundwater models takes especial importance.
	To a lesser degree of sophistication and as an immediate route, such tools
		may aid in the formulation of -- or may formulate themselves --
		regular~expressions which reduce the manuals to a form closer to that
		needed by the {TF}-{IDF} analyser.
	A likely pool from which to draw such techniques resides in the GeoDeepDive
		project \cite{zhang13}.
	The current centralised model for the revision control system of the
		{G}eoscience {S}tandard~{N}ames acts unwieldily.
	Were authority over the {GSN} to be distributed but hierarchical, progress
		in the extension of this ontology would hasten.
	In this manner, any geoscientist could fashion new publicly available
		standard~names as their need arises; at a time after creation and
		immediate availability could a steward of the ontology certify that the
		standard~name is indeed warranted.\footnote{The use of deprecated
		variables would even be supported by a redirection which accounts for
		its version and branch.}

	\end{multicols}


	{
		\bibliographystyle{plainnat}
		\bibliography{./sds-csc-paper.bib}
	}


	\begin{appendices}

		\chapter{Computer Code}
			\label{ch:code}

			\section{\MODFLOW{}-2005 to \ParFlow{} Crosswalk}
			\label{sec:crosswalk}
			\lstinputlisting[												%
				style=code,													%
				language={Perl},											%
			]{../../mod2par/crosswalk/bin/CSVMatch.pl}

			\section{{TF}-{IDF} and Cosine~Similarity Module}
			\label{sec:tfidf}
			\lstinputlisting[												%
				style=code,													%
				language={Perl},											%
			]{../../mod2par/crosswalk/bin/TFIDFSim.pm}

		\chapter{Licenses}
			\label{ch:licenses}

			\section{{M}odified~{BSD} License}
			\label{sec:bsd}
			\lstinputlisting[												%
				style=license,												%
			]{../licenses/License-BSD-Modified.txt}

			\section{{C}reative~{C}ommons {A}ttribution 4.0 {I}nternational
				{P}ublic~{L}icense}
			\label{sec:ccbysa}
			\lstinputlisting[												%
				style=license,												%
			]{../licenses/License-CC-BY-4_0.txt}

	\end{appendices}


\end{document}
