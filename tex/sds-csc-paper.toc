\contentsline {chapter}{Abstract}{i}{Doc-Start}
\contentsline {chapter}{Acknowledgements}{ii}{chapter*.1}
\contentsline {paragraph}{}{ii}{chapter*.1}
\contentsline {paragraph}{}{ii}{chapter*.1}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Materials and Methods}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Failings with \textsc {FloPerl}{}}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Towards Efficient Conversion Input/Output}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}A Step-Back -- Limitations in the Original Converters}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}A Workflow -- Up-Conversion of Legacy Models}{8}{section.2.4}
\contentsline {chapter}{\numberline {3}Results}{10}{chapter.3}
\contentsline {chapter}{\numberline {4}Discussion}{12}{chapter.4}
\contentsline {chapter}{Literature Cited}{14}{chapter*.4}
\contentsline {chapter}{Reflection}{16}{chapter*.5}
\contentsline {chapter}{Appendices}{17}{section*.6}
\contentsline {chapter}{\numberline {A}Computer Code}{18}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}\textsc {modflow}{} Up-Conversion}{18}{section.1.A.1}
\contentsline {subsection}{\numberline {A.1.1}Workflow}{18}{subsection.1.A.1.1}
\contentsline {subsubsection}{\numberline {A.1.1.1}ZIP Archive Unnesting}{21}{subsubsection.1.A.1.1.1}
\contentsline {subsubsection}{\numberline {A.1.1.2}Up-Conversion and Simulation}{22}{subsubsection.1.A.1.1.2}
\contentsline {subsection}{\numberline {A.1.2}Patches}{23}{subsection.1.A.1.2}
\contentsline {subsubsection}{\numberline {A.1.2.1}\textsc {modflow}{}-96 to -2000 -- Without HFB}{23}{subsubsection.1.A.1.2.1}
\contentsline {subsubsection}{\numberline {A.1.2.2}\textsc {modflow}{}-96 to -2000 -- With HFB}{30}{subsubsection.1.A.1.2.2}
\contentsline {subsubsection}{\numberline {A.1.2.3}\textsc {modflow}{}-2000 to -2005}{43}{subsubsection.1.A.1.2.3}
\contentsline {section}{\numberline {A.2}\textsc {modflow}{}-2005 to \textsc {ParFlow}{} Conversion}{43}{section.1.A.2}
\contentsline {subsection}{\numberline {A.2.1}\textsc {modflow}{}-2005 Input Modules}{43}{subsection.1.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}\textsc {modflow}{}-2005 to \textsc {ParFlow}{} Crosswalk}{50}{subsection.1.A.2.2}
\contentsline {subsubsection}{\numberline {A.2.2.1}TF-IDF and Cosine Similarity Module}{51}{subsubsection.1.A.2.2.1}
\contentsline {subsection}{\numberline {A.2.3}\textsc {ParFlow}{} Output Modules}{54}{subsection.1.A.2.3}
\contentsline {subsection}{\numberline {A.2.4}\textsc {FloPerl}{}}{79}{subsection.1.A.2.4}
\contentsline {subsubsection}{\numberline {A.2.4.1}Two-Dimensional Integer Array Reader}{90}{subsubsection.1.A.2.4.1}
\contentsline {subsubsection}{\numberline {A.2.4.2}Real Array Reader}{94}{subsubsection.1.A.2.4.2}
\contentsline {subsubsection}{\numberline {A.2.4.3}Miscellaneous Subroutines}{98}{subsubsection.1.A.2.4.3}
\contentsline {chapter}{\numberline {B}Licenses}{99}{Appendix.1.B}
\contentsline {section}{\numberline {B.1}{M}odified\nobreakspace {}{BSD}\nobreakspace {}{L}icense}{99}{section.1.B.1}
\contentsline {section}{\numberline {B.2}{C}reative\nobreakspace {}{C}ommons Attribution 4.0 International {P}ublic\nobreakspace {}{L}icense}{99}{section.1.B.2}
